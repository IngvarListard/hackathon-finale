from hashlib import sha256

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, _user_has_perm, UserManager
from django.db import models
from django.utils.crypto import get_random_string, constant_time_compare
import re


def username_validate(value):
    # TODO Не более 250 символов.
    pattern = compile('^[\w.,!?@+_\'"|/&№«»„“)(–↔ -]+$')
    is_valid = pattern.match(value)
    return is_valid


class UserType(models.Model):
    name = models.CharField('Тип пользователя', max_length=20)


class Picture(models.Model):
    link = models.TextField('Ссылка')
    file = models.ImageField('Файл')


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('ФИО пользователя', max_length=250)
    birth_date = models.DateField('Дата рождения', null=True)
    email = models.CharField('Электронная почта', max_length=64, null=True, default=None, blank=True, unique=True)
    password = models.CharField(max_length=64)
    last_online = models.DateTimeField('Последний заход', null=True)
    user_type = models.ManyToManyField('users.UserType')
    rating = models.PositiveIntegerField('Рейтинг', default=0)
    avatar = models.ForeignKey(Picture, null=True, on_delete=models.CASCADE)
    subscriptions = models.ManyToManyField('self', verbose_name='Подпичсчики', blank=True)
    bio = models.TextField('Биография', null=True)
    track_tags = models.ManyToManyField('blog.Tag')

    salt = models.CharField(max_length=64)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'

    class Meta:
        permissions = (
            ('is_jury', 'Сотрудник член жури'),
        )

    objects = UserManager()

    def set_password(self, raw_password):
        salt = get_random_string(64)
        hasher = sha256()
        raw_password = raw_password + '_' + salt
        hasher.update(raw_password.encode('utf-8'))
        self.salt = salt
        self.password = hasher.hexdigest()
        return self

    def check_password(self, raw_password):
        hasher = sha256()
        raw_password = raw_password + '_' + self.salt
        hasher.update(raw_password.encode('utf-8'))
        result = constant_time_compare(hasher.hexdigest(), self.password)
        return result

    def has_perm(self, perm, obj=None):
        # Суперпользователи имеют весь доступ
        if self.is_superuser:
            return True
        return _user_has_perm(self, perm, obj)

    @property
    def is_staff(self):
        return self.is_superuser


class Region(models.Model):
    name = models.CharField(max_length=250)
