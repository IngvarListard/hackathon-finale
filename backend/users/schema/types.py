import graphene
import graphene_django
import django

from backend.users.models import User
from backend.users.models import Region


class UserType(graphene.Interface):

    class Meta:
        description = 'Абстрактный интерфейс пользователя'

    id = graphene.Int(required=True)
    username = graphene.String()
    # first_name = graphene.String()
    # last_name = graphene.String()
    login = graphene.String()
    email = graphene.String()
    is_superuser = graphene.String()


class BasicUserType(graphene_django.DjangoObjectType):

    class Meta:
        model = User
        interface = (UserType,)
        description = 'Объект пользователя с базовой информацией'
        only_fields = ('id', 'username', 'is_superuser', 'email', 'bio')


class RegionType(graphene_django.DjangoObjectType):
    class Meta:
        model = Region


# noinspection PyUnusedLocal
class FullUserType(graphene_django.DjangoObjectType):

    class Meta:
        model = User
        interfaces = (UserType,)
        description = 'Объект пользователя с полной информацией '
        exclude_fields = ['password', 'salt']


class ApplicantsType(graphene.ObjectType):
    sum_likes = graphene.Int()

    class Meta:
        model = User

