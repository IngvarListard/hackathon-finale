import graphene
from django.contrib.auth import authenticate, login, logout
from graphene_file_upload.scalars import Upload

from backend.users.models import User, Picture
from backend.users.schema.types import BasicUserType
from backend.users.schema.types import UserType


# noinspection PyMethodMayBeStatic
class CreateUser(graphene.Mutation):
    new_user = graphene.Field(UserType)

    class Arguments:
        username = graphene.String(required=True)
        email = graphene.String(required=True)
        birth_date = graphene.Date(required=True)
        password = graphene.String(required=True)
        user_type_id = graphene.String(required=True)

    def mutate(self, info, **kwargs):
        password = kwargs.pop('password')
        new_user = User.objects.create(username=kwargs['username'], email=kwargs['email'],
                                       birth_date=kwargs['birth_date'])
        new_user.user_type.add(kwargs['user_type_id'])
        new_user.set_password(password)
        new_user.save()
        return CreateUser(new_user=new_user)


# noinspection PyMethodMayBeStatic
class EditUser(graphene.Mutation):
    user = graphene.Field(UserType)

    class Input:
        user_id = graphene.Int(required=True)
        username = graphene.String()
        email = graphene.String()
        birth_date = graphene.Date()
        bio = graphene.String()

    def mutate(self, info, user_id, username=None, email=None, birth_date=None, bio=None):
        edit_user = User.objects.get(id=user_id)

        if username:
            edit_user.username = username
        if email:
            edit_user.email = email
        if birth_date:
            edit_user.birth_date = birth_date
        if bio:
            edit_user.bio = bio
        edit_user.save()
        return EditUser(user=edit_user)


class AddTagToTrack(graphene.Mutation):
    result = graphene.Boolean()

    class Arguments:
        tag_id = graphene.String(required=True)

    def mutate(self, info, tag_id):
        from backend.blog.models import Tag
        curent_user = info.context.user

        try:
            tag = Tag.objects.get(id=tag_id)
        except Tag.DoesNotExist:
            raise Exception('Ошибка: тег не найден')

        curent_user.track_tags.add(tag)
        return AddTagToTrack(result=True)


class RemoveTagFromTrack(graphene.Mutation):
    result = graphene.Boolean()

    class Arguments:
        tag_id = graphene.String(required=True)

    def mutate(self, info, tag_id):
        from backend.blog.models import Tag
        curent_user = info.context.user

        try:
            tag = Tag.objects.get(id=tag_id)
        except Tag.DoesNotExist:
            raise Exception('Ошибка: тег не найден')

        curent_user.track_tags.remove(tag)
        return RemoveTagFromTrack(result=True)


# noinspection PyMethodMayBeStatic
class Login(graphene.Mutation):
    user = graphene.Field(BasicUserType)
    success = graphene.Boolean()

    class Meta:
        description = 'Вход пользователя в систему'

    class Arguments:
        login = graphene.String(required=True)
        password = graphene.String(required=True)

    def mutate(self, info, **kwargs):
        print(kwargs)
        user = authenticate(info.context, username=kwargs['login'], password=kwargs['password'])
        if user is not None:
            login(info.context, user)
        return Login(user=user, success=user is not None)


# noinspection PyMethodMayBeStatic
class Logout(graphene.Mutation):
    class Meta:
        description = 'Выход пользователя из системы'

    success = graphene.Boolean(required=True, description='Успех операции')

    @staticmethod
    def mutate(root, info):
        if info.context.user.is_authenticated:
            logout(info.context)
            return Logout(success=True)
        else:
            return Logout(success=False)


# noinspection PyMethodMayBeStatic
class UploadFile(graphene.Mutation):

    class Arguments:
        file = Upload(required=True)

    success = graphene.Boolean()

    def mutate(self, info, file, **kwargs):
        print(file)
        return UploadFile(success=True)


class UploadAvatar(graphene.Mutation):

    class Arguments:
        file = Upload(required=True)

    success = graphene.String()

    def mutate(self, info, file, **kwargs):
        print('hello')
        user = info.context.user
        user = User.objects.get(id=user.id)
        if user.avatar:
            user.avatar.file.delete()
        picture = Picture.objects.create(file=file, link='')
        user.avatar = picture
        user.save()
        return UploadFile(success="Cool")


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    edit_user = EditUser.Field()
    add_tag_to_track = AddTagToTrack.Field()
    remove_tag_from_track = RemoveTagFromTrack.Field()
    login = Login.Field()
    logout = Logout.Field()
    file_upload = UploadFile.Field()
    avatar_upload = UploadAvatar.Field()
