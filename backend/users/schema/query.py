import graphene
import graphene_django
from backend.users.schema.types import BasicUserType
from backend.users.models import UserType


class UserTypeType(graphene_django.DjangoObjectType):
    class Meta:
        model = UserType


class Query(graphene.ObjectType):
    all_users = graphene.List(BasicUserType)
    get_current_user = graphene.Field(BasicUserType)
    get_user_types = graphene.List(UserTypeType)

    def resolve_all_users(self, info):
        from backend.users.models import User
        from backend.notifications.tasks import test_task
        raise ValueError('AAAAAAA')
        test_task.delay(1, 2)
        return User.objects.all()

    def resolve_get_current_user(self, info):
        user = info.context.user if info.context.user.is_authenticated else None
        return user

    def resolve_get_user_types(self, info):
        return UserType.objects.all()


