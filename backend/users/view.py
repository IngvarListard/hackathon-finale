import mimetypes
from django.http import HttpResponse, HttpResponseRedirect
from backend.users.models import User


def download(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    user_id = request.GET.get('user_id')
    user = User.objects.get(id=user_id)
    if user.avatar:
        picture = user.avatar
        file = picture.file
        name = file.name
        content_type = mimetypes.guess_type(name)[0]
        response = HttpResponse(file.read(), content_type=content_type)
        # 'attachment; filename="filename"' - Сразу скачивает файл без открытия
        # 'filename="filename"' - Открывает файл в отдельной вкладке
        response['Content-Disposition'] = ('filename="%s"' % name).encode('utf-8')
        return response
    else:
        return HttpResponse()
