import graphene

from backend.users.models import Region
from backend.users.schema.types import RegionType
from backend.blog.models import Tag
from backend.blog.schema.types import TagType
from backend.blog.schema.types import ArticleType
from django.core.paginator import Paginator

from backend.blog.schema.types import PagedArticles


class Query(graphene.ObjectType):
    get_all_regions = graphene.List(RegionType)
    get_all_tags = graphene.List(TagType)
    get_article = graphene.Field(ArticleType, article_id=graphene.Int(required=True))
    get_paged_articles = graphene.Field(
        PagedArticles,
        page=graphene.Int(required=True),
        offset=graphene.Int(required=True)
    )

    def resolve_get_all_regions(self, info):
        return Region.objects.all()

    def resolve_get_all_tags(self, info):
        return Tag.objects.all()

    def resolve_get_article(self, info, article_id):
        from backend.blog.models import Article
        return Article.objects.get(id=article_id)

    def resolve_get_paged_articles(self, info, page, offset):
        from backend.blog.models import Article
        articles = Article.objects.all()
        pages = Paginator(articles, offset)
        # articles = pages.page(page)
        # return Article.objects.all()
        return PagedArticles(articles=articles, total_count=pages.count)

