import graphene

from backend.blog.models import Article, Tag
from backend.blog.schema.types import ArticleInputType, ArticleType
from backend.users.models import User


class CreateArticle(graphene.Mutation):
    article_id = graphene.Int()

    class Arguments:
        article = ArticleInputType(required=True)

    def mutate(self, info, article):
        from django.core.mail import send_mail

        tags = article.pop('tags')
        region_id = article.pop('region')
        article_obj = Article(**article)
        # article.creator = info.context.user
        article_obj.creator = None
        article_obj.region_id = region_id
        article_obj.save()
        if tags:
            existing_tags = Tag.objects.filter(name__in=tags)
            existing_tags_names = set(existing_tags.in_bulk(field_name='name'))
            new_tags = Tag.objects.bulk_create(Tag(name=t) for t in set(tags) - existing_tags_names)
            article_obj.tags.add(*existing_tags, *new_tags)

        post_url = 'http://localhost:3000/article/' + str(article_obj.id)
        # Рассылка почты подписчикам
        for subscriber in info.context.user.subscriptions.all():
            send_mail('Новый пост на Dobro.ru',
                      'Пользователь {} написал новый пост "{}" \n {}. '.format(info.context.user.username, article['title'], post_url),
                      'prodobroMessage@yandex.ru', [subscriber.email])
        return CreateArticle(article_id=article_obj.id)


class DoLike(graphene.Mutation):
    success = graphene.Boolean()

    class Arguments:
        article_id = graphene.Int()

    def mutate(self, info, article_id):
        article = Article.objects.get(id=article_id)
        article.likes += 1
        article.save()
        return DoLike(success=True)


class Mutation(graphene.ObjectType):
    create_article = CreateArticle.Field()
    do_like = DoLike.Field()
