import graphene
import graphene_django
from graphene_file_upload.scalars import Upload

from backend.blog import models
from backend.blog.models import Tag


class ArticleInputType(graphene.InputObjectType):
    title = graphene.String(required=True)
    title_image = Upload()
    tags = graphene.List(graphene.String)
    text = graphene.String(required=True)
    region = graphene.ID()
    article_type = graphene.ID()
    participants = graphene.List(graphene.ID)
    headline = graphene.String()


class ArticleType(graphene_django.DjangoObjectType):

    class Meta:
        model = models.Article


class RegionRESTType(graphene.ObjectType):
    id = graphene.ID()
    name = graphene.String()
    full_pretty_name = graphene.String()


class TagType(graphene_django.DjangoObjectType):

    class Meta:
        model = Tag


class PagedArticles(graphene.ObjectType):
    articles = graphene.List(ArticleType)
    total_count = graphene.Int()
