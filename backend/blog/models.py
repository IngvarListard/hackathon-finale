from django.contrib.postgres import fields
from django.db import models


class Article(models.Model):
    title = models.CharField(name='title', max_length=250)
    title_image = models.ImageField(upload_to='images/', null=True, blank=True)
    text = models.TextField(default='')
    tags = models.ManyToManyField('blog.Tag')
    likes = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    creator = models.ForeignKey('users.User', on_delete=models.PROTECT, null=True, blank=True)
    region = models.ForeignKey('users.Region', on_delete=models.SET_NULL, null=True)
    article_type = models.ForeignKey('blog.ArticleType', on_delete=models.SET_NULL, null=True)
    participants = fields.ArrayField(models.IntegerField(blank=True, null=True), null=True, blank=True)
    create_article_date = models.DateTimeField('Дата создания', auto_now_add=True, null=True)
    headline = models.CharField(max_length=200, blank=True, null=True)


class Tag(models.Model):
    name = models.CharField('name', max_length=50, unique=True)


class ArticleType(models.Model):
    name = models.CharField(max_length=250)


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    parent = models.ForeignKey('blog.Comment', blank=True, null=True, on_delete=models.CASCADE)
    creator = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(auto_now_add=True)
    likes = models.IntegerField(default=0)


class Picture(models.Model):
    image = models.ImageField(upload_to='images/')
