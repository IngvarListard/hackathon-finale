from django.db import migrations
import requests

def populate_regions(apps, schema):
    region = apps.get_model('users', 'Region')
    uri = 'https://api.dobrf.ru/open-api/v1/regions/'
    regions = requests.get(uri).json()
    r_to_bd = []
    for r in regions:
        r_to_bd.append(region(
            id=r['id'],
            name=r['full_pretty_name']
        ))
    region.objects.bulk_create(r_to_bd)



class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20190928_1142'),
    ]

    operations = [
        migrations.RunPython(populate_regions)
    ]
