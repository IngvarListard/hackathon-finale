from django.apps import AppConfig


class GrantConfig(AppConfig):
    name = 'backend.grant'
