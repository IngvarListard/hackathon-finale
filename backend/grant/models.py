from django.db import models
from backend.users.models import User


class CategoryType(models.Model):
    name = models.CharField(max_length=64)


class StageType(models.Model):
    name = models.CharField(max_length=64)


class Company(models.Model):
    name = models.CharField(max_length=64)


class Grant(models.Model):
    name = models.CharField(max_length=128)
    amount = models.FloatField()
    from_company = models.ForeignKey(Company, on_delete=models.PROTECT, related_name='created_grants')
    jury = models.ManyToManyField(User, null=True, blank=True, related_name='jury_grants')
    winner = models.ForeignKey(User, on_delete=models.PROTECT, related_name='won_grants', null=True)
    participants = models.ManyToManyField(User, related_name='grants')
    stage = models.ForeignKey(StageType, on_delete=models.PROTECT)
    closed = models.BooleanField(default=False)
    filtered_users = models.ManyToManyField(User, related_name='filtered')
    grant_date = models.DateField('Дата созданий гранта', auto_now_add=True, null=True)
    title = models.CharField('Заголовок', max_length=100, null=True)
    description = models.TextField('Описание', null=True)


class UserGrantEstimate(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    grant = models.ForeignKey(Grant, on_delete=models.PROTECT)
    points = models.IntegerField('Оценка судьи')
    responsible_jury = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')


class Applicants(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    grant_date = models.DateField('Дата номинации', auto_now_add=True)
    old = models.BooleanField('Бывший претендент', default=False)
    viewer_likes = models.ManyToManyField(User, verbose_name='Одобрение пользователей', related_name='+')
    # Ваше очко уходит в зрительный зал
    viewer_winner = models.BooleanField('Победитель зрительского голосования', default=False)
