import graphene
import requests
from backend.grant.schema.types import GrantType
from backend.grant.models import Grant
import json

# import blog.schema.types


class Query(graphene.ObjectType):
    get_volunteers = graphene.Field(
        graphene.String,
        search=graphene.String(required=True),
        count=graphene.String(),
        page=graphene.String()
    )
    get_events = graphene.Field(
        graphene.String,
        search=graphene.String(required=True),
        count=graphene.String(),
        page=graphene.String()
    )
    get_grants = graphene.List(GrantType)

    get_grant = graphene.Field(GrantType, grant_id=graphene.String(required=True))

    get_regions = graphene.List('backend.blog.schema.types.RegionRESTType', search=graphene.String())

    def resolve_get_grant(self, info, grant_id):
        try:
            gr = Grant.objects.get(id=grant_id)
            return Grant.objects.get(id=grant_id)
        except Grant.DoesNotExist:
            raise Exception('Ошибка: такого гранта на существует.')

    def resolve_get_volunteers(self, info, search='', count=15, page=1):
        site = 'https://api.dobrf.ru/volunteers/?'
        params = 'search={}&page_size={}&page={}'.format(search, count, page)
        r = requests.get(site+params)
        return r.text

    def resolve_get_events(self, info, search='', count=15, page=1):
        site = 'https://api.dobrf.ru/events/?'
        params = 'search={}&page_size={}&page={}'.format(search, count, page)
        r = requests.get(site+params)
        return r.text

    def resolve_get_grants(self, info):
        return Grant.objects.all()[:10]

    def resolve_get_regions(self, info, search=''):
        uri = 'https://api.dobrf.ru/open-api/v1/regions/'
        r = requests.get(uri, params={'search': search})
        return r.json()
