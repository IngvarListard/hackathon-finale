import graphene
from backend.users.models import User
from backend.grant.models import Grant, UserGrantEstimate


class EntryGrantRequest(graphene.Mutation):
    result = graphene.Boolean()

    class Arguments:
        grant_id = graphene.String(required=True)

    def mutate(self, info, grant_id):
        current_user = info.context.user
        try:
            user = User.objects.get(id=current_user.id)
        except User.DoesNotExist:
            raise Exception('Такой пользователь не найден')
        try:
            grant = Grant.objects.get(id=grant_id)
        except User.DoesNotExist:
            raise Exception('Такой грант не найден')
        grant.participants.add(user)
        return EntryGrantRequest(result=True)


# Предфильтрация претендентов на грант
class FilterGrantParticipants(graphene.Mutation):
    result = graphene.Boolean()

    class Arguments:
        grant_id = graphene.String(required=True)
        participant_id = graphene.String(required=True)

    def mutate(self, info, grant_id, participant_id):
        try:
            user = User.objects.get(id=participant_id)
        except User.DoesNotExist:
            raise Exception('Такой пользователь не найден')
        try:
            grant = Grant.objects.get(id=grant_id)
        except User.DoesNotExist:
            raise Exception('Такой грант не найден')

        grant.filtered_users.add(user)

        # grant.participants.remove(user)
        return FilterGrantParticipants(result=True)


class SetPointsUser(graphene.Mutation):
    result = graphene.Boolean()

    class Arguments:
        user_id = graphene.String(required=True)
        grant_id = graphene.String(required=True)
        points = graphene.Int(required=True)

    def mutate(self, info, user_id, grant_id):
        try:
            User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise Exception('Такой пользователь не найден')
        try:
            Grant.objects.get(id=grant_id)
        except User.DoesNotExist:
            raise Exception('Такой грант не найден')

        UserGrantEstimate.objects.get_or_create(user_id=user_id, grant_id=grant_id, responsible_jury_id=info.context.user.id)



        return SetPointsUser(result=True)


class Mutation(graphene.ObjectType):
    entry_grant_request = EntryGrantRequest()
    filter_grant_participants = FilterGrantParticipants()
