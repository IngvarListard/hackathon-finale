import graphene_django
from backend.grant.models import Grant


class GrantType(graphene_django.DjangoObjectType):
    class Meta:
        model = Grant
