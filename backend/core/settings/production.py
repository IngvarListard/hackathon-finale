"""
Django settings for marketplace project.

Generated by 'django-admin startproject' using Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

# from backend.core.utils import get_bool_from_env
from backend.core.celery_beat_schedule import CELERY_BEAT_SCHEDULE

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '^m1yh^j1xm1ut(1*=3ae3l$2ugv#d1dohxjm1e3z(odca*)tcf'
# SECRET_KEY = os.environ.get('SECRET_KEY')

GRAPHQL_BATCH = True
# TODO сменить
ALLOWED_HOSTS = [
    *os.environ.get('ALLOWED_WEB_HOSTS', 'localhost').strip('"').split(' '),
]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'channels',
    'graphene_django',
    'django_celery_beat',
    'backend.users.apps.UsersConfig',
    'backend.notifications.apps.NotificationsConfig',
    'backend.grant.apps.GrantConfig',
    'backend.blog'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'backend.core.middleware.is_authenticated_middleware.IsUserAuthenticatedMiddleware',
]

ROOT_URLCONF = 'backend.core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'backend.core.wsgi.application'
ASGI_APPLICATION = 'backend.core.routing.application'

REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 6379)

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [(REDIS_HOST, REDIS_PORT)],
        },
    },
}

# Celery
CELERY_RESULT_BACKEND = 'redis://{}/0'.format(REDIS_HOST)
CELERY_BROKER_URL = 'redis://marketplace@{}:{}//'.format(REDIS_HOST, REDIS_PORT)
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Volgograd'

GRAPHENE = {
    'SCHEMA': 'backend.core.api.schema',
    'MIDDLEWARE': [
        # 'crm.api.AuthorizationMiddleware'
    ]
}

# Настройки отправки почты
EMAIL_HOST = 'smtp.yandex.com'
EMAIL_HOST_USER = 'prodobroMessage@yandex.ru'
EMAIL_HOST_PASSWORD = 'uBE-QXx-6GY-i53'
EMAIL_USE_TLS = True


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME', 'marketplace'),
        'USER': os.environ.get('DB_USER', 'marketplace'),
        'PASSWORD': os.environ.get('DB_PASSWORD', 'marketplace'),
        'HOST': os.environ.get('DB_HOST', 'localhost'),
        'PORT': os.environ.get('DB_PORT', '5432'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[{levelname}][{asctime}][{module}][{funcName}]: {message}',
            'style': '{',
        },
        'simple': {
            'format': '[{levelname}][{asctime}]: {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './django.log',
            'formatter': 'verbose',
            # 'filters': ['require_file_log_true']
        },
        # 'mail_admins': {
        #     'level': 'ERROR',
        #     'class': 'django.utils.log.AdminEmailHandler',
        #     'formatter': 'verbose',
        #     'filters': ['require_debug_false']
        # },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        # 'require_file_log_true': {
        #     '()': 'synergycrm.log.RequireFileLogTrue',
        # }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'production': {
            'handlers': ['console', 'file'],
            'level': 'WARNING',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}

ENABLE_FILE_LOG = os.environ.get('ENABLE_FILE_LOG', False)
ENABLE_DB_LOG = os.environ.get('ENABLE_DB_LOG', False)


AUTH_USER_MODEL = 'users.User'

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Volgograd'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.environ.get(BASE_DIR, 'static')  # работает, но я не знаю почему

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../../media')  # работает, но я не знаю почему

# Куки
SESSION_COOKIE_AGE = 3600 * 12  # Кука сессии живет 12 часов
MAX_UPLOAD_SIZE = 50*1024*1024
FILE_UPLOAD_MAX_MEMORY_SIZE = 50*1024*1024
