from celery.schedules import crontab

CELERY_BEAT_SCHEDULE = {
    # 'add-every-30-seconds': {
    #     'task': 'backend.notifications.tasks.periodic_test_task',
    #     'schedule': 30.0,
    #     'args': ('test', 'another test')
    # },
    'set_new_applicants': {
        'task': 'backend.core.tasks.set_new_applicants',
        'schedule': crontab(0, 0, day_of_month='1', month_of_year='*/1')
    },
    'viewer_win_applicant': {
        'task': 'backend.core.tasks.viewer_win_applicant',
        'schedule': crontab(0, 0, day_of_month='5', month_of_year='*/1')
    },
}
