from .celery import app
from django.db import transaction
from backend.users.models import User
from backend.blog.models import Article
from django.db.models import Sum, Subquery, OuterRef, F, Value, IntegerField, Count
import datetime
from backend.users.schema.types import ApplicantsType
from backend.grant.models import Applicants
from django.db.models import functions


@app.task
def add(x, y):
    return x + y


# Определение прентендентов на грант в конце месяца
@app.task
@transaction.atomic
def set_new_applicants():
    now = datetime.datetime.now()
    like_count = Article.objects.filter(creator_id=OuterRef('id'), create_article_date__year=now.year,
                                        create_article_date__month=now.month)\
        .values('creator_id').annotate(sum=functions.Coalesce(
        Sum('likes'), Value(0), output_field=IntegerField())).values('sum')
    applicants = User.objects.annotate(
        sum_likes=functions.Coalesce(Subquery(like_count), Value(0.0), output_field=IntegerField())).order_by('-sum_likes')[:2]

    for applicant in applicants.all():
        Applicants.objects.create(user_id=applicant.id)


# Определение победителя зрительского голосования пятого числа каждого месяца
@app.task
@transaction.atomic
def viewer_win_applicant():
    import operator

    applicants = Applicants.objects.filter(old=False)

    if applicants.count() > 0:
        applicants_dict = {d: d.viewer_likes.count() for d in Applicants.objects.filter(old=False)}
        applicant_winner = max(applicants_dict.items(), key=operator.itemgetter(1))[0]

        applicant_winner.viewer_winner = True
        applicant_winner.save()
        applicants.update(old=True)
