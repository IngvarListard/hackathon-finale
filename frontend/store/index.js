/* eslint-disable no-console */
import Vue from 'vue'
import { generateUid } from '@/utils'
import { GET_PAGED_ARTICLES } from '../graphql/query'

export const state = () => ({
  articles: [],
  offset: 12,
  page: 1,
  totalCount: 0,
  drawer: false,
  items: [
    {
      text: 'Home',
      to: '/'
    },
    {
      text: 'About',
      href: '#about'
    }
  ],
  errors: [],
  notifications: [],
  socket: {
    isConnected: false,
    message: '',
    reconnectError: false
  }
})

export const mutations = {
  doLike({ articles }, articleId) {
    const article = articles.find(a => a.id === articleId)
    article.likes++
  },
  pushError({ errors }, error) {
    errors.push(error)
  },
  shiftError: ({ errors }) => errors.shift(),
  hideError({ errors }, id) {
    const error = errors.find(e => e.id === id)
    if (error) {
      error.show = false
    }
  },
  showError({ errors }, id) {
    const error = errors.find(e => e.id === id)
    if (error) {
      error.show = true
    }
  },

  setDrawer: (state, payload) => (state.drawer = payload),
  toggleDrawer: state => (state.drawer = !state.drawer),
  setPage: (state, { page, offset, articles, totalCount }) => {
    state.page = page
    state.offset = offset
    state.articles = articles
    state.totalCount = totalCount
    // Object.assign(state, payload)
  }
}

export const getters = {
  errorsGetter: ({ errors }) => errors,

  links: (state, getters) => {
    return state.items.concat(getters.categories)
  },
  articles: state => state.articles
}

export const actions = {
  pushError(ctx, error) {
    const id = generateUid()
    ctx.commit('pushError', { id, text: error, show: false })
    setTimeout(() => {
      ctx.commit('showError', id)
    }, 50)
    setTimeout(() => {
      ctx.commit('hideError', id)
    }, 5000)
  },
  async nuxtServerInit({ dispatch }, ctx) {
    const isLoggedIn = ctx.app.$cookies.get('isLoggedIn')
    if (isLoggedIn) {
      await dispatch('auth/getCurrentUser')
    }
  },
  getPagedArticles({ commit }, { page, offset }) {
    return this.app.apolloProvider.defaultClient
      .query({
        query: GET_PAGED_ARTICLES,
        variables: {
          page,
          offset
        }
      })
      .then(({ data: { getPagedArticles: { articles, totalCount } } }) => {
        console.log('ARTICLES', articles)
        console.log('TOTALCOUNT', totalCount)
        commit('setPage', { page, offset, articles, totalCount })
      })
  }
}
