import gql from 'graphql-tag'

const GET_ARTICLE = gql`
  query($articleId: Int!) {
    article: getArticle(articleId: $articleId) {
      id
      title
      titleImage
      text
      headline
      tags {
        id
        name
      }
      likes
      views
      creator {
        id
        username
      }
      region {
        id
        name
      }
      participants
      createArticleDate
    }
  }
`

const GET_PAGED_ARTICLES = gql`
  query($page: Int!, $offset: Int!) {
    getPagedArticles(page: $page, offset: $offset) {
      articles {
        id
        title
        titleImage
        headline
        createArticleDate
        text
        tags {
          id
          name
        }
        likes
        views
        creator {
          id
          username
        }
        region {
          id
          name
        }
        participants
      }
      totalCount
    }
  }
`

export { GET_ARTICLE, GET_PAGED_ARTICLES }
