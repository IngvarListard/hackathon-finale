import gql from 'graphql-tag'

const login = gql`
  mutation($login: String!, $password: String!) {
    login(login: $login, password: $password) {
      user {
        id
        isSuperuser
        username
        email
        bio
      }
      success
    }
  }
`

const logout = gql`
  mutation {
    logout {
      success
    }
  }
`

export { login, logout }
