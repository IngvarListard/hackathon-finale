import gql from 'graphql-tag'

const allUsers = gql`
  {
    allUsers {
      id
      username
    }
  }
`
const getCurrentUser = gql`
  {
    getCurrentUser {
      id
      isSuperuser
      username
      email
      bio
    }
  }
`

export { allUsers, getCurrentUser }
