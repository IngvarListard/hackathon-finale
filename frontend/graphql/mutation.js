import gql from 'graphql-tag'

const CREATE_ARTICLE = gql`
  mutation($article: ArticleInputType!) {
    createArticle(article: $article) {
      articleId
    }
  }
`

export { CREATE_ARTICLE }
