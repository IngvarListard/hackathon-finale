import gql from 'graphql-tag'

const utilsMixin = {
  methods: {
    getVolunteers(search) {
      return this.$apollo
        .query({
          query: gql`
            query($search: String!) {
              getVolunteers(search: $search)
            }
          `,
          fetchPolicy: 'no-cache',
          variables: {
            search: search
          }
        })
        .then(({ data }) => {
          return JSON.parse(data.getVolunteers)
        })
    },
    getEvents(search) {
      return this.$apollo
        .query({
          query: gql`
            query($search: String!) {
              getEvents(search: $search)
            }
          `,
          fetchPolicy: 'no-cache',
          variables: {
            search: search
          }
        })
        .then(({ data }) => {
          return JSON.parse(data.getEvents)
        })
    },
    formatMediaUrl(uri) {
      if (!uri) return ''
      return `http://localhost:8000/media/${uri}`
    },
    formatDate(textDate) {
      const moment = require('moment')
      moment.locale('ru-ru')
      if (textDate) {
        return moment(textDate).format('L')
      } else {
        return ''
      }
    }
  }
}

export default utilsMixin
